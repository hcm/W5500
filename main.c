#include "webserver.h"
#include "w5500.h"

void main(void)
{
	IT0 = 1;  		//负跳变中断触发
	EX0 = 1; 	    //打开外部中断
	EA = 1; 		//打开终中断
	w5500_hardware_reset();  	//W5500硬复位，通过引脚RST电平触发
	w5500_init(); 				//w5500初始化
	socket_init(SOCKET0, 80); 	//初始化SOCKET
	socket_listen(SOCKET0); 	//监听
	while(1) {
		if(W5500_IRQMARK_ISSET()) 	//检测中断标志
			w5500_irq_process(); 	//IRQ处理过程
	}
}

//中断处理，使用1寄存器组压栈
void INT0_irq(void) interrupt 0 using 1
{
	//通过设置一个变量来触发w5500_irq_process函数
	//中断处理时间不能太长，不然很影响性能，
	//所以用标志变量触发Main函数的中断处理函数
	//w5500.h
	EA = 0;
	W5500_SET_IRQMARK();
	EA = 1;
}